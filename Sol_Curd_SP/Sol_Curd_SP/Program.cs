﻿using Sol_Curd_SP.Entity;
using Sol_Curd_SP.Repository;
using Sol_Curd_SP.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Curd_SP
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async() =>
            {
                #region  insert section
                IUserRepository userRepositoryObj = new UserRepository();
                //await userRepositoryObj.InsertData(new UserEntity()
                //{
                //    FirstName = "diwik",
                //    LastName = "suvarna",
                //    userLoginEntityObj = new UserLoginEntity()
                //    {
                //        Username = "diwik",
                //        Password = "4868"
                //    },
                //    userCommunicationEntityObj = new UserCommunicationEntity()
                //    {
                //        MobileNo = "8108940723",
                //        EmailId = "diwik@gmail.com"
                //    }
                //});

                #endregion

                #region  update section
                //await userRepositoryObj.UpdateData(new UserEntity()
                //{
                //    UserId=4,
                //    FirstName = "tanush",
                //    LastName = "suvarna",
                //    userLoginEntityObj = new UserLoginEntity()
                //    {
                //        Username = "tanush",
                //        Password = "986"
                //    },
                //    userCommunicationEntityObj = new UserCommunicationEntity()
                //    {
                //        MobileNo = "8108944723",
                //        EmailId = "tanush@gmail.com"
                //    }
                //});
                #endregion

                #region  delete section
                await userRepositoryObj.DeleteData(new UserEntity()
                {
                    UserId = 1                   
                });
                #endregion

            }).Wait();

        }
    }
}
