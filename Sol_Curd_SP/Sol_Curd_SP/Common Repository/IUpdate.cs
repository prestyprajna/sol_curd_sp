﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Curd_SP.Common_Repository
{
    public interface IUpdate<TEntity> where TEntity : class
    {
        Task<Boolean> UpdateData(TEntity entityObj);
    }
}
