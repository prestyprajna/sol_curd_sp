﻿using Sol_Curd_SP.Concrete.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sol_Curd_SP.Entity.Interface;
using Sol_Curd_SP.EF;
using System.Data.Entity.Core.Objects;

namespace Sol_Curd_SP.Concrete
{
    public class UserConcrete : IUserConcrete
    {
        #region  declaration
        private UserDBEntities db = null;

        #endregion

        #region  constructor

        public UserConcrete()
        {
            db = new UserDBEntities();
        }

        #endregion

        #region  public methods

        public async Task<dynamic> SetData(string command,IUserEntity entityObj,Action<int?,string> actionStoredProcOut=null)
        {
            try
            {
                ObjectParameter status = null;
                ObjectParameter message = null;

                return await Task.Run(() =>
                {
                    var setQuery =
                    db.uspSetUser(
                        command,
                        entityObj?.UserId,
                        entityObj?.FirstName,
                        entityObj?.LastName,
                        entityObj?.userLoginEntityObj?.Username,
                        entityObj?.userLoginEntityObj?.Password,
                        entityObj?.userCommunicationEntityObj?.MobileNo,
                        entityObj?.userCommunicationEntityObj?.EmailId,
                        status = new ObjectParameter("Status", typeof(int)),
                        message = new ObjectParameter("Message", typeof(string))
                        );

                    actionStoredProcOut(Convert.ToInt32(status.Value), message.Value.ToString());

                    return setQuery;

                });
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        #endregion

       
    }
}
