﻿using Sol_Curd_SP.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Curd_SP.Entity
{
    public class UserLoginEntity: IUserLoginEntity
    {
        public int UserId { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
