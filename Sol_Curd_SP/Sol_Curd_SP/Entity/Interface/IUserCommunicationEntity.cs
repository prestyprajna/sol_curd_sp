﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Curd_SP.Entity.Interface
{
    public interface IUserCommunicationEntity
    {
         int UserId { get; set; }

         string MobileNo { get; set; }

         string EmailId { get; set; }
    }
}
