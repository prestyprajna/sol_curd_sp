﻿using Sol_Curd_SP.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Curd_SP.Entity
{
    public class UserCommunicationEntity: IUserCommunicationEntity
    {
        public int UserId { get; set; }

        public string MobileNo { get; set; }

        public string EmailId { get; set; }
    }
}
