﻿using Sol_Curd_SP.Common_Repository;
using Sol_Curd_SP.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Curd_SP.Repository.Interface
{
    public interface IUserRepository : IInsert<IUserEntity>, IUpdate<IUserEntity>, IDelete<IUserEntity>
    {
    }
}
