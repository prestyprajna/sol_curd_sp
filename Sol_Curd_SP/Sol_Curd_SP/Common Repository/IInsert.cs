﻿using Sol_Curd_SP.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Curd_SP.Common_Repository
{
    public interface IInsert<TEntity> where TEntity:class
    {
        Task<Boolean> InsertData(TEntity entityObj);
    }
}
