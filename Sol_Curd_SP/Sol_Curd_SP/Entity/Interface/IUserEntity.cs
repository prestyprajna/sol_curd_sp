﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Curd_SP.Entity.Interface
{
    public interface IUserEntity
    {
        int UserId { get; set; }
        
        string FirstName { get; set; }

        string LastName { get; set; }

        IUserLoginEntity userLoginEntityObj { get; set; }

        IUserCommunicationEntity userCommunicationEntityObj { get; set; }
    }
}
