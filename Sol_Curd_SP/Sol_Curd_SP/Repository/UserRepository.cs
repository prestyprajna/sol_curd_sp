﻿using Sol_Curd_SP.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sol_Curd_SP.Entity.Interface;
using Sol_Curd_SP.Concrete.Interface;
using Sol_Curd_SP.Concrete;

namespace Sol_Curd_SP.Repository
{
    public class UserRepository : IUserRepository
    {
        #region  declaration
        private IUserConcrete userConcreteObj = null;

        #endregion

        #region  constructor

        public UserRepository()
        {
            userConcreteObj = new UserConcrete();
        }

        #endregion

        public async Task<bool> InsertData(IUserEntity entityObj)
        {
            try
            {
                int? status = null;
                string message = null;

                await userConcreteObj.SetData(
                    "Insert",
                    entityObj,
                    (leStatus,leMessage)=>
                    {
                        status = leStatus;
                        message = leMessage;
                    });

                return (status==1)?true:false;
        
            }

            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> UpdateData(IUserEntity entityObj)
        {
            try
            {
                int? status = null;
                string message = null;

                await userConcreteObj.SetData(
                    "Update",
                    entityObj,
                    (leStatus, leMessage) =>
                    {
                        status = leStatus;
                        message = leMessage;
                    });

                return (status == 1) ? true : false;

            }

            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> DeleteData(IUserEntity entityObj)
        {
            try
            {
                int? status = null;
                string message = null;

                await userConcreteObj.SetData(
                    "Delete",
                    entityObj,
                    (leStatus, leMessage) =>
                    {
                        status = leStatus;
                        message = leMessage;
                    });

                return (status == 1) ? true : false;

            }

            catch (Exception)
            {
                throw;
            }
        }    

    }
}
