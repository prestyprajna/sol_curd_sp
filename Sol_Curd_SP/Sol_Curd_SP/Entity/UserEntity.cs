﻿using Sol_Curd_SP.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Curd_SP.Entity
{
    public class UserEntity:IUserEntity
    {
        public int UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public IUserLoginEntity userLoginEntityObj { get; set; }

        public IUserCommunicationEntity userCommunicationEntityObj { get; set; }
    }
}
