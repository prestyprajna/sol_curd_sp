﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Curd_SP.Common_Repository
{
    public interface ISetData<TEntity> where TEntity:class
    {
        Task<dynamic> SetData(string command,TEntity entityObj, Action<int?, string> actionStoredProcOut = null);
    }
}
